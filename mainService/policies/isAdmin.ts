import { Request, Response, NextFunction } from "express";

export default (req: Request, res: Response, next: NextFunction) => {
  if (req.user.role === "admin") {
    // User is admin
    next();
  } else {
    // User is not admin
    res.status(403).send({
      success: false,
      message: "You do not have access."
    });
  }
};
