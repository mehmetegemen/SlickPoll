export default {
    'MAIN_MONGO_URL': process.env.MONGO_URL,
    'PORT': process.env.PORT || 3002,
    'USERS_SERVICE_URL': process.env.USERS_SERVICE_URL,
    'SECRET': process.env.SECRET,
    'MAIL_CREDENTIALS': {
        auth: {
            api_key: 'xxxxxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxxxx-xxxxxxxxx'
            || process.env.MAIL_API_KEY,
            domain: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.mailgun.org'
            || process.env.MAIL_DOMAIN
        }
    },
    'HOST_ADDRESS': process.env.HOST_ADDRESS || 'http://localhost:8080/'
}