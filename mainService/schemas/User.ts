import { Document, Schema, Model, model } from "mongoose";
import { IUser } from "../interfaces/User";

export interface IUserModel extends IUser, Document {}

export let UserSchema: Schema = new Schema({
  birthDate: Date,
  firstName: String,
  familyName: String,
  password: String,
  email: String,
  provider: String,
  accessToken: String,
  role: { type: String, default: "not_verified" }
});

export const User: Model<IUserModel> = model<IUserModel>("User", UserSchema);
