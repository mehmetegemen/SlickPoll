import express, { Request, Response, NextFunction } from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import debug from 'debug'
import fs from 'fs'
import cors from 'cors'
import config from './config/config'
import http from 'http'
import routes from './routes'


const app = express();
const server = http.createServer(app);

const log = debug('poll:app');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,' + 
    ' Content-Type, Accept');
    next();
});
app.use(cors());
app.use(express.static(path.join(__dirname, 'public')));

// Handle errors, serve them as JSON
app.use(function(error: any, req: Request, res: Response, next: NextFunction) {
    if (req.app.get('env') === 'development') {
        res.status(500).json(
            {
                success: false,
                message: error.message
            }
        );
    } else {
        res.render('error', {message: 'Service error'});
    }
});

routes(app); // Initiate routes

server.listen(config.PORT || 3003);


export default app;