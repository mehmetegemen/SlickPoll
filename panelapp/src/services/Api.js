import axios from 'axios'
import store from '../store/index'

export default () => {
    return axios.create({
        baseURL: 'http://localhost:3002/',
        headers: {
          'Content-Type': `application/json`,
          Authorization: `Bearer ${store.state.token}`
        }
    });
}