import Api from './Api'

export default {
  create (data) {
    return Api().put('poll/create', data)
  },
  update (data) {
    return Api().post('poll/update', data)
  },
  listOf (data) {
    return Api().get('poll/listByOwner/' + data.skip + '/' + data.limit)
  },
  find (id) {
    return Api().get('poll/findByPollId/' + id)
  },
  delete (id) {
    return Api().delete('poll/destroy/' + id)
  }
}